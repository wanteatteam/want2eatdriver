package jdroidcoder.ua.wanteatdriver;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jdroidcoder on 07.08.17.
 */
public class User {
    @SerializedName("id")
    private static Integer id;
    @SerializedName("role_id")
    private static String roleId;
    @SerializedName("shop_id")
    private static Integer shopId;
    @SerializedName("first_name")
    private static String firstName;
    @SerializedName("last_name")
    private static String lastName;
    @SerializedName("phone")
    private static String phone;
    @SerializedName("email")
    private static String email;
    @SerializedName("token")
    private static String token;

    public static Integer getId() {
        return id;
    }

    public static void setId(Integer id) {
        User.id = id;
    }

    public static String getRoleId() {
        return roleId;
    }

    public static void setRoleId(String roleId) {
        User.roleId = roleId;
    }

    public static Integer getShopId() {
        return shopId;
    }

    public static void setShopId(Integer shopId) {
        User.shopId = shopId;
    }

    public static String getFirstName() {
        return firstName;
    }

    public static void setFirstName(String firstName) {
        User.firstName = firstName;
    }

    public static String getLastName() {
        return lastName;
    }

    public static void setLastName(String lastName) {
        User.lastName = lastName;
    }

    public static String getPhone() {
        return phone;
    }

    public static void setPhone(String phone) {
        User.phone = phone;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        User.email = email;
    }

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        User.token = token;
        WantEatApplication.token = token;
    }

    public static String getFirstLastName() {
        return getFirstName() + " " + getLastName();
    }
}
