package jdroidcoder.ua.wanteatdriver;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import lombok.Getter;

/**
 * Created by jdroidcoder on 18.07.17.
 */
public class WantEatApplication extends Application {
    private static WantEatApplication application;
    public static String token;
    public static String login;
    public static String password;
    public static String operatorPhone;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        application = this;
    }

    public static WantEatApplication getApplication() {
        return application;
    }
}