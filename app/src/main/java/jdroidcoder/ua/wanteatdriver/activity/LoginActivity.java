package jdroidcoder.ua.wanteatdriver.activity;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jdroidcoder.ua.wanteatdriver.R;
import jdroidcoder.ua.wanteatdriver.User;
import jdroidcoder.ua.wanteatdriver.WantEatApplication;
import jdroidcoder.ua.wanteatdriver.presenter.LoginPresenter;
import jdroidcoder.ua.wanteatdriver.presenter.impl.LoginPresenterImpl;
import jdroidcoder.ua.wanteatdriver.utils.Util;
import jdroidcoder.ua.wanteatdriver.view.LoginView;

public class LoginActivity extends AppCompatActivity implements LoginView {
    @BindView(R.id.enter)
    CardView enterCardView;
    @BindView(R.id.login)
    EditText loginEditText;
    @BindView(R.id.password)
    EditText passwordEditText;
    @BindView(R.id.login_text_input_layout)
    TextInputLayout loginTextInputLayout;
    @BindView(R.id.password_text_input_layout)
    TextInputLayout passwordTextInputLayout;
    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        if(WantEatApplication.token!=null){
            logged();
            return;
        }
        User.setToken(Util.loadToken(this));
        if (loginPresenter == null)
            loginPresenter = new LoginPresenterImpl(this, this);
    }

    @OnClick(R.id.enter)
    public void login() {
        loginPresenter.login();
    }

    @Override
    public String getLogin() {
        return loginEditText.getText().toString();
    }

    @Override
    public String getPassword() {
        return passwordEditText.getText().toString();
    }

    @Override
    public void logged() {
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void invalidLogin() {
        loginTextInputLayout.setError(getString(R.string.invalid_login));
    }

    @Override
    public void invalidPassword() {
        passwordTextInputLayout.setError(getString(R.string.invalid_password));
    }
}