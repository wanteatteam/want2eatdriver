package jdroidcoder.ua.wanteatdriver.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jdroidcoder.ua.wanteatdriver.R;
import jdroidcoder.ua.wanteatdriver.User;
import jdroidcoder.ua.wanteatdriver.WantEatApplication;
import jdroidcoder.ua.wanteatdriver.adapter.ProductAdapter;
import jdroidcoder.ua.wanteatdriver.adapter.ViewPagerAdapter;
import jdroidcoder.ua.wanteatdriver.event.ChangeLocation;
import jdroidcoder.ua.wanteatdriver.event.HideCallToClientButton;
import jdroidcoder.ua.wanteatdriver.event.HideMapButton;
import jdroidcoder.ua.wanteatdriver.event.HideNotification;
import jdroidcoder.ua.wanteatdriver.event.HideOrderButton;
import jdroidcoder.ua.wanteatdriver.event.ShowCallToClientButton;
import jdroidcoder.ua.wanteatdriver.event.ShowMapButton;
import jdroidcoder.ua.wanteatdriver.event.ShowOrderButton;
import jdroidcoder.ua.wanteatdriver.event.ShowOrderForClient;
import jdroidcoder.ua.wanteatdriver.event.ShowOrderForRestaurant;
import jdroidcoder.ua.wanteatdriver.event.UpdateTabs;
import jdroidcoder.ua.wanteatdriver.fragment.BottomSheetFragmentDialog;
import jdroidcoder.ua.wanteatdriver.fragment.FreeStreamFragment;
import jdroidcoder.ua.wanteatdriver.fragment.MyOrderFragment;
import jdroidcoder.ua.wanteatdriver.fragment.NewOrderFragment;
import jdroidcoder.ua.wanteatdriver.network.RetrofitConfig;
import jdroidcoder.ua.wanteatdriver.network.RetrofitSubscriber;
import jdroidcoder.ua.wanteatdriver.network.ValidateServerResponseOrThrow;
import jdroidcoder.ua.wanteatdriver.presenter.MainPresenter;
import jdroidcoder.ua.wanteatdriver.presenter.impl.MainPresenterImpl;
import jdroidcoder.ua.wanteatdriver.response.MainResponse;
import jdroidcoder.ua.wanteatdriver.service.TrackingOrderService;
import jdroidcoder.ua.wanteatdriver.socket.SocketManager;
import jdroidcoder.ua.wanteatdriver.view.MainView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by jdroidcoder on 08.08.17.
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        MainView, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, TabLayout.OnTabSelectedListener {
    private static final int REQUEST_CODE_LOCATION = 102;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    TextView firstLastName;
    TextView email;
    @BindView(R.id.order)
    TextView order;
    @BindView(R.id.route)
    TextView route;
    @BindView(R.id.call_to_client)
    TextView callToClient;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    private BottomSheetDialogFragment bottomSheetDialogFragment = new BottomSheetFragmentDialog();
    private ViewPagerAdapter adapter;
    private GoogleApiClient mGoogleApiClient;
    private boolean isLogout = false;
    private MainPresenter mainPresenter;
    private SocketManager socketManager;
    private LocationRequest mLocationRequest;
    private Fragment fragment = null;
    public static Location lastLocation;
    public static String address = "";
    private String clientNumber = "";
    private FreeStreamFragment freeStreamFragment;
    private MyOrderFragment myOrderFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (mainPresenter == null)
            mainPresenter = new MainPresenterImpl(this, this);
        ButterKnife.bind(this);
        if (socketManager == null)
            socketManager = new SocketManager(this);
        socketManager.openConnection();
        initToolbar();
        initGoogleClient();
        EventBus.getDefault().register(this);
        initTabs();
        startService(new Intent(this, TrackingOrderService.class));
        if (!checkLocationServicesEnabled(this)) {
            displayNoLocationProviderDialog();
        }
        getOperatorPhone();
    }

    public void getOperatorPhone() {
        RetrofitConfig.getAdapter().getOperatorPhone(null)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .flatMap(new ValidateServerResponseOrThrow<>())
                .subscribe(new RetrofitSubscriber<MainResponse>() {
                    @Override
                    public void onNext(MainResponse main) {
                        WantEatApplication.operatorPhone = main.getOperatorPhone();
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                    }
                });
    }

    private void initTabs() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(freeStreamFragment = FreeStreamFragment.newInstance(),
                getString(R.string.free_stream_label).toUpperCase());
        adapter.addFragment(myOrderFragment = MyOrderFragment.newInstance(),
                getString(R.string.my_orders_label).toUpperCase());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(this);
    }

    private void displayNoLocationProviderDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(R.string.ens_message_error_locations_disabled)
                .setPositiveButton(R.string.settings, (dialog, which) -> {
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    dialog.dismiss();
                })
                .setNegativeButton(R.string.cancel, (dialog, which) -> {
                    dialog.dismiss();
                })
                .show();
    }

    public static boolean checkLocationServicesEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!statusOfGPS)
                statusOfGPS = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            return statusOfGPS;
        }
        return false;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (tab.getPosition() == 0) {
            try {
                freeStreamFragment.presenter.loadOrders();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if (tab.getPosition() == 1) {
            try {
                myOrderFragment.presenter.loadOrders();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Subscribe
    public void onUpdateTabs(UpdateTabs updateTabs) {
        freeStreamFragment.presenter.loadOrders();
        myOrderFragment.presenter.loadOrders();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void initGoogleClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        firstLastName = ButterKnife.findById(headerView, R.id.first_last_name);
        firstLastName.setText(User.getFirstLastName());
        email = ButterKnife.findById(headerView, R.id.email);
        email.setText(User.getEmail());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_logout) {
            mainPresenter.logout();
            isLogout = true;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Subscribe
    public void onShowOrderButton(ShowOrderButton showOrderButton) {
        order.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.order)
    public void showOrderForRestaurant() {
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        if (User.getToken() != null) {
            socketManager.closeConnection();
        }
        super.onDestroy();
    }

    @Override
    public void logout() {
        mGoogleApiClient.disconnect();
        stopService(new Intent(this, TrackingOrderService.class));
        mainPresenter.logout();
        finish();
        if (isLogout)
            startActivity(new Intent(this, LoginActivity.class));
    }

    @Override
    public void showNewOrder(Fragment fragment) {
        this.fragment = fragment;
        getSupportFragmentManager().beginTransaction().replace(android.R.id.content, fragment)
                .addToBackStack(fragment.getClass().getName()).commitAllowingStateLoss();
    }

    @Override
    public void removeFragment() {
        try {
            if (this.fragment != null) {
                getSupportFragmentManager().beginTransaction().remove(this.fragment).commit();
                this.fragment = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(10000);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CODE_LOCATION);
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (lastLocation == null) {
            lastLocation = location;
        } else {
            System.out.println(gps2m(lastLocation.getLatitude(), lastLocation.getLongitude(),
                    location.getLatitude(), location.getLongitude()));
            if (gps2m(lastLocation.getLatitude(), lastLocation.getLongitude(),
                    location.getLatitude(), location.getLongitude()) >= 0) {
                String json = "{\"method\":\"update_gps\",\"id\":" + User.getId() + "," +
                        "\"lat\":" + location.getLatitude() + ",\"lng\":" + location.getLongitude() + "}";
                if (socketManager != null && socketManager.getSocketConnection().isConnected())
                    socketManager.getSocketConnection().sendTextMessage(json);
            }
            lastLocation = location;
        }
        EventBus.getDefault().post(new ChangeLocation());
    }

    public int gps2m(double latA, double lngA, double latB, double lngB) {
        double pk = (180 / 3.14169);

        double a1 = latA / pk;
        double a2 = lngA / pk;
        double b1 = latB / pk;
        double b2 = lngB / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        return (int) (6366000 * tt);
    }

    @OnClick(R.id.route)
    public void showMap() {
        startActivity(new Intent(this, MapActivity.class));
    }

    @Subscribe
    public void onShowMapButton(ShowMapButton showMapButton) {
        route.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void onHideMapButton(HideMapButton hideMapButton) {
        route.setVisibility(View.GONE);
    }

    @Subscribe
    public void onHideOrderButton(HideOrderButton hideOrderButton) {
        order.setVisibility(View.GONE);
    }

    @OnClick(R.id.call_to_client)
    public void callToClient() {
        Intent callIntentM = new Intent(Intent.ACTION_DIAL);
        callIntentM.setData(Uri.parse("tel:" + clientNumber));
        startActivity(callIntentM);
    }

    @OnClick(R.id.call_to_operator)
    public void callToOperator() {
        Intent callIntentM = new Intent(Intent.ACTION_DIAL);
        callIntentM.setData(Uri.parse("tel:" + WantEatApplication.operatorPhone));
        startActivity(callIntentM);
    }

    @Subscribe
    public void onHideCallToClientButton(HideCallToClientButton hideCallToClientButton) {
        callToClient.setVisibility(View.GONE);
        clientNumber = "";
    }

    @Subscribe
    public void onShowCallToClientButton(ShowCallToClientButton showCallToClientButton) {
        callToClient.setVisibility(View.VISIBLE);
        clientNumber = showCallToClientButton.getClientNumber();
    }

    @Subscribe
    public void onHideNotification(HideNotification hideNotification) {
        for (int i = 0; i < getSupportFragmentManager().getFragments().size(); i++) {
            try {
                if (Objects.equals(((NewOrderFragment) getSupportFragmentManager().getFragments().get(i)).getOrderId()
                        , hideNotification.getOrderId())) {
                    getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().getFragments().get(i))
                            .commitNowAllowingStateLoss();
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
