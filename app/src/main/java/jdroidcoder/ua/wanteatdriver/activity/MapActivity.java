package jdroidcoder.ua.wanteatdriver.activity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.UnsupportedEncodingException;
import java.util.List;

import jdroidcoder.ua.wanteatdriver.R;
import jdroidcoder.ua.wanteatdriver.event.ChangeLocation;
import jdroidcoder.ua.wanteatdriver.map.DirectionFinder;
import jdroidcoder.ua.wanteatdriver.map.DirectionFinderListener;
import jdroidcoder.ua.wanteatdriver.response.RouteResponse;

/**
 * Created by jdroidcoder on 06.09.17.
 */
public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, DirectionFinderListener {
    private GoogleMap mMap;
    private Polyline polylines;
    private Marker myLocationMarker;
    private Marker startLocationMarker;
    private Marker point;
    private String address;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_map);
        EventBus.getDefault().register(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void sendRequest() {
        String origin = "";
        if (MainActivity.lastLocation != null)
            origin = "" + MainActivity.lastLocation.getLatitude() + ", " + MainActivity.lastLocation.getLongitude();
        else
            origin = "Волошина, 5, Рівне, Рівненська область, 33000";
        String destination = MainActivity.address;
        try {
            new DirectionFinder(this, origin, destination).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(this);
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        sendRequest();
    }

    @Override
    public void onDirectionFinderStart() {
    }

    @Override
    public void onDirectionFinderSuccess(List<RouteResponse> routes) {
        mMap.clear();
        for (RouteResponse route : routes) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.getStartLocation(), 16));

            myLocationMarker = mMap.addMarker(new MarkerOptions()
                    .icon(vectorToBitmap(R.drawable.ic_person_pin_circle_black_24dp))
                    .title(route.getStartAddress())
                    .position(route.getStartLocation()));
            startLocationMarker = mMap.addMarker(new MarkerOptions()
                    .icon(vectorToBitmap(R.drawable.ic_flag_black_24dp))
                    .title(route.getStartAddress())
                    .position(route.getStartLocation()));
            point = mMap.addMarker(new MarkerOptions()
                    .icon(vectorToBitmap(R.drawable.ic_store_small_grey))
                    .title(route.getEndAddress())
                    .position(route.getEndLocation()));

            PolylineOptions polylineOptions = new PolylineOptions()
                    .geodesic(true)
                    .color(Color.parseColor("#5B8E4A"))
                    .width(10);

            for (int i = 0; i < route.getPoints().size(); i++)
                polylineOptions.add(route.getPoints().get(i));

            polylines = mMap.addPolyline(polylineOptions);
        }
    }

    @Subscribe
    public void onChangeLocation(ChangeLocation location) {
        myLocationMarker.setPosition(new LatLng(MainActivity.lastLocation.getLatitude(), MainActivity.lastLocation.getLongitude()));
    }

    private BitmapDescriptor vectorToBitmap(@DrawableRes int id) {
        Drawable vectorDrawable = ResourcesCompat.getDrawable(getResources(), id, null);
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
