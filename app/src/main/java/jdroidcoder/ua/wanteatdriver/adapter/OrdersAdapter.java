package jdroidcoder.ua.wanteatdriver.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jdroidcoder.ua.wanteatdriver.R;
import jdroidcoder.ua.wanteatdriver.fragment.AcceptedOrderFragment;
import jdroidcoder.ua.wanteatdriver.network.RetrofitConfig;
import jdroidcoder.ua.wanteatdriver.network.RetrofitSubscriber;
import jdroidcoder.ua.wanteatdriver.network.ValidateServerResponseOrThrow;
import jdroidcoder.ua.wanteatdriver.response.MainResponse;
import jdroidcoder.ua.wanteatdriver.response.OrderResponse;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by jdroidcoder on 17.09.17.
 */
public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {
    private List<OrderResponse> orders = new ArrayList<>();
    private Context context;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private boolean isFreeStream = true;

    public OrdersAdapter(boolean isFreeStream) {
        this.isFreeStream = isFreeStream;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item_style, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OrderResponse order = orders.get(position);
        holder.setOrder(order);
        holder.orderNumber.setText("№ " + order.getOrderId());
        if (order.getStatusId().equals("shop_order_accept")) {
            holder.shopName.setText(order.getShop().getName());
            holder.shopAddress.setText(order.getShop().getStreet() + " " + order.getShop().getBuild());
            holder.money.setText(context.getString(R.string.currency, order.getTotalPriceForShop()));
            try {
                holder.time.setText(timeFormat.format(dateFormat.parse(order.getFinishCookAt())));
            } catch (ParseException e) {
                holder.time.setVisibility(View.INVISIBLE);
                e.printStackTrace();
            }
        } else if (order.getStatusId().equals("shop_order_get_for_courier")) {
            holder.shopName.setText(order.getClientAddress().getClient().getFirstName() + " " + order.getClientAddress().getClient().getLastName());
            holder.shopAddress.setText(order.getClientAddress().getStreet() + " " + order.getClientAddress().getBuild());
            holder.money.setText(context.getString(R.string.currency, order.getTotalPriceForClient()));
            try {
                holder.time.setText(timeFormat.format(dateFormat.parse(order.getFinishDeliveryAt())));
            } catch (ParseException e) {
                holder.time.setVisibility(View.INVISIBLE);
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public void setItems(List<OrderResponse> orders) {
        this.orders.clear();
        this.orders.addAll(orders);
        this.notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.order_number)
        TextView orderNumber;
        @BindView(R.id.money)
        TextView money;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.shop_name)
        TextView shopName;
        @BindView(R.id.shop_address)
        TextView shopAddress;
        OrderResponse order;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public OrderResponse getOrder() {
            return order;
        }

        public void setOrder(OrderResponse order) {
            this.order = order;
        }

        @OnClick(R.id.container)
        public void showDetails() {
            if (!isFreeStream) {
                ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction()
                        .replace(android.R.id.content, AcceptedOrderFragment.newInstance(order))
                        .addToBackStack(AcceptedOrderFragment.class.getName())
                        .commit();
            } else {
                getOperatorPhone();
            }
        }

        private void getOperatorPhone() {
            RetrofitConfig.getAdapter().getOperatorPhone(order.getOrderId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .flatMap(new ValidateServerResponseOrThrow<>())
                    .subscribe(new RetrofitSubscriber<MainResponse>() {
                        @Override
                        public void onNext(MainResponse main) {
                            Intent callIntentM = new Intent(Intent.ACTION_DIAL);
                            callIntentM.setData(Uri.parse("tel:" + main.getOperatorPhone()));
                            context.startActivity(callIntentM);
                        }

                        @Override
                        public void onError(Throwable e) {
                            super.onError(e);
                        }
                    });
        }
    }
}
