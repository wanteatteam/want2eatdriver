package jdroidcoder.ua.wanteatdriver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jdroidcoder.ua.wanteatdriver.R;
import jdroidcoder.ua.wanteatdriver.response.ProductResponse;

/**
 * Created by jdroidcoder on 24.07.17.
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    private List<ProductResponse> products = new ArrayList<>();
    private Context context;
    private boolean isForShop = true;

    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item_style, parent, false));
    }

    @Override
    public void onBindViewHolder(ProductAdapter.ViewHolder holder, int position) {
        ProductResponse product = products.get(position);
        holder.productName.setText(product.getProductName());
        int count = product.getCount();
        holder.productCount.setText(context.getString(R.string.count, count));
        if (isForShop) {
            holder.productSizePrice.setText("(" + product.getSize() + product.getProductUnitName() +
                    "/" + context.getString(R.string.currency, product.getPriceBase()) + ")");
            double totalProductPrice = product.getPriceBase() * count;
            holder.productTotalPrice.setText(context.getString(R.string.currency, totalProductPrice));
        } else {
            holder.productSizePrice.setText("(" + product.getSize() + product.getProductUnitName() +
                    "/" + context.getString(R.string.currency, product.getPrice()) + ")");
            double totalProductPrice = product.getPrice() * count;
            holder.productTotalPrice.setText(context.getString(R.string.currency, totalProductPrice));
        }
        if (product.getPackPrice() > 0) {
            holder.packCount.setText(context.getString(R.string.count, count));
            double totalPackPrice = product.getPackPrice() * count;
            holder.packTotalPrice.setText(context.getString(R.string.currency, totalPackPrice));
            holder.packPrice.setText(context.getString(R.string.currency, product.getPackPrice()));
            holder.packItem.setVisibility(View.VISIBLE);
        } else {
            holder.packItem.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public void setItems(List<ProductResponse> products) {
        this.products.clear();
        this.products.addAll(products);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.product_name)
        TextView productName;
        @BindView(R.id.product_size_price)
        TextView productSizePrice;
        @BindView(R.id.product_count)
        TextView productCount;
        @BindView(R.id.pack_price)
        TextView packPrice;
        @BindView(R.id.pack_count)
        TextView packCount;
        @BindView(R.id.pack_total_price)
        TextView packTotalPrice;
        @BindView(R.id.product_total_price)
        TextView productTotalPrice;
        @BindView(R.id.pack_item)
        LinearLayout packItem;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setForShop(boolean forShop) {
        isForShop = forShop;
    }
}
