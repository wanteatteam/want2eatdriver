package jdroidcoder.ua.wanteatdriver.event;

/**
 * Created by jdroidcoder on 20.09.17.
 */
public class HideNotification {
    private Integer orderId;

    public HideNotification(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }
}
