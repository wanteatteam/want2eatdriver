package jdroidcoder.ua.wanteatdriver.event;

/**
 * Created by jdroidcoder on 06.09.17.
 */
public class ShowCallToClientButton {
   private String clientNumber;

    public ShowCallToClientButton(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }
}
