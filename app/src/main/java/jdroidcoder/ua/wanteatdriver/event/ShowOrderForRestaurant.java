package jdroidcoder.ua.wanteatdriver.event;

import java.util.List;

import jdroidcoder.ua.wanteatdriver.response.ProductResponse;

/**
 * Created by jdroidcoder on 29.08.17.
 */
public class ShowOrderForRestaurant {
    private Double deliveryPrice;

    private List<ProductResponse> products;

    public ShowOrderForRestaurant(Double deliveryPrice, List<ProductResponse> products) {
        this.deliveryPrice = deliveryPrice;
        this.products = products;
    }

    public Double getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(Double deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    public List<ProductResponse> getProducts() {
        return products;
    }

    public void setProducts(List<ProductResponse> products) {
        this.products = products;
    }
}
