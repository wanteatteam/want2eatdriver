package jdroidcoder.ua.wanteatdriver.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import jdroidcoder.ua.wanteatdriver.R;
import jdroidcoder.ua.wanteatdriver.activity.MainActivity;
import jdroidcoder.ua.wanteatdriver.event.DefaultBottomMenu;
import jdroidcoder.ua.wanteatdriver.event.HideCallToClientButton;
import jdroidcoder.ua.wanteatdriver.event.HideMapButton;
import jdroidcoder.ua.wanteatdriver.event.HideOrderButton;
import jdroidcoder.ua.wanteatdriver.event.ShowCallToClientButton;
import jdroidcoder.ua.wanteatdriver.event.ShowMapButton;
import jdroidcoder.ua.wanteatdriver.event.ShowOrderButton;
import jdroidcoder.ua.wanteatdriver.event.ShowOrderForClient;
import jdroidcoder.ua.wanteatdriver.event.ShowOrderForRestaurant;
import jdroidcoder.ua.wanteatdriver.presenter.AcceptedOrderPresenter;
import jdroidcoder.ua.wanteatdriver.presenter.impl.AcceptedOrderPresenterImpl;
import jdroidcoder.ua.wanteatdriver.response.AddressResponse;
import jdroidcoder.ua.wanteatdriver.response.OrderResponse;
import jdroidcoder.ua.wanteatdriver.view.AcceptedOrderView;

/**
 * Created by jdroidcoder on 21.08.17.
 */
public class AcceptedOrderFragment extends BaseFragment implements AcceptedOrderView {
    private static final String ORDER_KEY = "order_key";

    public static AcceptedOrderFragment newInstance(OrderResponse order) {
        AcceptedOrderFragment acceptedOrderFragment = new AcceptedOrderFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ORDER_KEY, order);
        acceptedOrderFragment.setArguments(bundle);
        return acceptedOrderFragment;
    }

    @BindView(R.id.shop_name)
    TextView shopName;
    @BindView(R.id.shop_address)
    TextView shopAddress;
    @BindView(R.id.need_pay)
    TextView needPay;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.client_address)
    TextView clientAddress;
    @BindView(R.id.room_number)
    TextView roomNumber;
    @BindView(R.id.level_number)
    TextView levelNumber;
    @BindView(R.id.entrance_number)
    TextView entranceNumber;
    @BindView(R.id.domophone_number)
    TextView domophoneNumber;
    @BindView(R.id.on_place)
    Button onPlace;
    @BindView(R.id.take_from_restaurant)
    Button takeFromRestaurant;
    @BindView(R.id.room_container)
    LinearLayout roomContainer;
    @BindView(R.id.dec_line_1)
    ImageView decLine1;
    @BindView(R.id.level_container)
    LinearLayout levelContainer;
    @BindView(R.id.dec_line_2)
    ImageView decLine2;
    @BindView(R.id.entrance_container)
    LinearLayout entranceContainer;
    @BindView(R.id.dec_line_3)
    ImageView decLine3;
    @BindView(R.id.client_first_last_name)
    TextView clientFirstLastName;
    @BindView(R.id.domophone_container)
    LinearLayout domophoneContainer;
    @BindView(R.id.restaurant_address_container)
    LinearLayout restaurantAddressContainer;
    @BindView(R.id.time_label)
    TextView timeLabel;
    @BindView(R.id.need_pay_label)
    TextView needPayLabel;
    @BindView(R.id.order_number)
    TextView orderNumber;
    private OrderResponse order;
    private AcceptedOrderPresenter acceptedOrderPresenter;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_accepted_order, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        order = (OrderResponse) getArguments().getSerializable(ORDER_KEY);
        if (acceptedOrderPresenter == null)
            acceptedOrderPresenter = new AcceptedOrderPresenterImpl(getContext(), this);
        // TODO: 26.09.17  
        try {
            init(order);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init(OrderResponse order) {
        // TODO: 26.09.17
        try {
            shopName.setText(order.getShop().getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        shopAddress.setText(order.getShop().getStreet() + " " + order.getShop().getBuild());
        needPay.setText(getString(R.string.currency, order.getTotalPriceForShop()));
        orderNumber.setText(String.valueOf(order.getOrderId()));
        AddressResponse clientAddress = order.getClientAddress();
        clientFirstLastName.setText(clientAddress.getClient().getFirstName() + " " + clientAddress.getClient().getLastName());
        this.clientAddress.setText(clientAddress.getStreet() + " " + clientAddress.getBuild());
        if (clientAddress.getRoom() == null || clientAddress.getRoom().isEmpty()) {
            roomContainer.setVisibility(View.GONE);
            decLine1.setVisibility(View.GONE);
        } else {
            roomContainer.setVisibility(View.VISIBLE);
            decLine1.setVisibility(View.VISIBLE);
            roomNumber.setText(String.valueOf(clientAddress.getRoom()));
        }
        if (clientAddress.getLevel() == null || clientAddress.getLevel().isEmpty()) {
            levelContainer.setVisibility(View.GONE);
            decLine2.setVisibility(View.GONE);
        } else {
            levelContainer.setVisibility(View.VISIBLE);
            decLine2.setVisibility(View.VISIBLE);
            levelNumber.setText(String.valueOf(clientAddress.getLevel()));
        }
        if (clientAddress.getEntrance() == null || clientAddress.getEntrance().isEmpty()) {
            entranceContainer.setVisibility(View.GONE);
            decLine3.setVisibility(View.GONE);
        } else {
            entranceContainer.setVisibility(View.VISIBLE);
            decLine3.setVisibility(View.VISIBLE);
            entranceNumber.setText(String.valueOf(clientAddress.getEntrance()));
        }
        if (clientAddress.getDomophoneCode() == null || clientAddress.getDomophoneCode().isEmpty()) {
            domophoneContainer.setVisibility(View.GONE);
            decLine3.setVisibility(View.GONE);
        } else {
            domophoneContainer.setVisibility(View.VISIBLE);
            decLine3.setVisibility(View.VISIBLE);
            domophoneNumber.setText(String.valueOf(clientAddress.getDomophoneCode()));
        }
        if (order.getStatusId().equals("shop_order_get_for_courier")) {
            try {
                time.setText(timeFormat.format(dateFormat.parse(order.getFinishDeliveryAt())));
            } catch (ParseException e) {
                time.setVisibility(View.INVISIBLE);
                e.printStackTrace();
            }
//            hideTakeFromRestaurant();
            timeLabel.setText(getString(R.string.need_will_be_at_client_label));
            needPayLabel.setText(getString(R.string.need_pay_for_client_label));
            needPay.setText(getString(R.string.currency, order.getTotalPriceForClient()));
            takeFromRestaurant.setVisibility(View.GONE);
            restaurantAddressContainer.setVisibility(View.GONE);
            onPlace.setVisibility(View.VISIBLE);
            EventBus.getDefault().post(new ShowOrderForClient());
            EventBus.getDefault().post(new ShowMapButton());
            EventBus.getDefault().post(new ShowCallToClientButton(order.getClientAddress().getClient().getPhone()));
            EventBus.getDefault().post(new ShowOrderForRestaurant(order.getDeliveryPrice(), order.getProducts()));
            EventBus.getDefault().post(new ShowOrderButton());
            MainActivity.address = order.getClientAddress().getStreet() + ", "
                    + order.getClientAddress().getBuild() + "Рівне, Рівненська область, 33000";
        } else {
            try {
                time.setText(timeFormat.format(dateFormat.parse(order.getFinishCookAt())));
            } catch (ParseException e) {
                time.setVisibility(View.INVISIBLE);
                e.printStackTrace();
            }
            EventBus.getDefault().post(new ShowMapButton());
            EventBus.getDefault().post(new ShowCallToClientButton(order.getClientPhone()));
            EventBus.getDefault().post(new ShowOrderForRestaurant(order.getDeliveryPrice(), order.getProducts()));
            EventBus.getDefault().post(new ShowOrderButton());
            MainActivity.address = order.getShop().getStreet() + ", "
                    + order.getShop().getBuild() + "Рівне, Рівненська область, 33000";
        }
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().post(new DefaultBottomMenu());
        EventBus.getDefault().post(new HideOrderButton());
        EventBus.getDefault().post(new HideCallToClientButton());
        EventBus.getDefault().post(new HideMapButton());
        super.onDestroyView();
    }

    @Override
    public OrderResponse getOrder() {
        return order;
    }

    @Override
    public void hideTakeFromRestaurant() {
        order.setStatusId("shop_order_get_for_courier");
        init(order);
        needPayLabel.setText(getString(R.string.need_pay_for_client_label));
        needPay.setText(getString(R.string.currency, order.getTotalPriceForClient()));
        takeFromRestaurant.setVisibility(View.GONE);
        restaurantAddressContainer.setVisibility(View.GONE);
        onPlace.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.take_from_restaurant)
    public void takeFromRestaurant() {
        EventBus.getDefault().post(new ShowOrderForClient());
        acceptedOrderPresenter.takeFromRestaurant();
    }

    @OnClick(R.id.on_place)
    public void onPlace() {
        acceptedOrderPresenter.onPlace();
        EventBus.getDefault().post(new HideOrderButton());
        EventBus.getDefault().post(new HideMapButton());
        EventBus.getDefault().post(new HideCallToClientButton());
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    @OnClick(R.id.back_button)
    public void back() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @OnClick(R.id.container)
    public void emptyClick() {

    }
}
