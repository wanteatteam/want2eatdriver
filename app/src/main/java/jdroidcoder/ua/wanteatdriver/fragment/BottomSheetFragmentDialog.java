package jdroidcoder.ua.wanteatdriver.fragment;

import android.app.Dialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.ButterKnife;
import jdroidcoder.ua.wanteatdriver.R;
import jdroidcoder.ua.wanteatdriver.adapter.ProductAdapter;
import jdroidcoder.ua.wanteatdriver.event.DefaultBottomMenu;
import jdroidcoder.ua.wanteatdriver.event.ShowOrderForClient;
import jdroidcoder.ua.wanteatdriver.event.ShowOrderForRestaurant;

/**
 * Created by jdroidcoder on 17.09.17.
 */
public class BottomSheetFragmentDialog extends BottomSheetDialogFragment {
    private RecyclerView productList;
    private TextView deliveryPrice;
    private ProductAdapter productAdapter = new ProductAdapter();
    private ShowOrderForRestaurant showOrderForRestaurant;
    private boolean isShoDelivery = false;

    public BottomSheetFragmentDialog() {
        super();
        EventBus.getDefault().register(this);
    }

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.bottom_sheet, null);
        productList = ButterKnife.findById(contentView, R.id.product_list);
        deliveryPrice = ButterKnife.findById(contentView, R.id.delivery_price);
        productAdapter.setItems(showOrderForRestaurant.getProducts());
        productList.setLayoutManager(new LinearLayoutManager(getActivity()));
        productList.setAdapter(productAdapter);
        deliveryPrice.setText(getString(R.string.delivery_price, showOrderForRestaurant.getDeliveryPrice()));
        if (isShoDelivery)
            deliveryPrice.setVisibility(View.VISIBLE);
        dialog.setContentView(contentView);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Subscribe
    public void onShowOrderForRestaurant(ShowOrderForRestaurant showOrderForRestaurant) {
        this.showOrderForRestaurant = showOrderForRestaurant;
    }

    @Subscribe
    public void onShowOrderForClient(ShowOrderForClient showOrderForClient) {
        isShoDelivery = true;
        productAdapter.setForShop(false);
        productAdapter.notifyDataSetChanged();
    }

    @Subscribe
    public void onDefaultBottomMenu(DefaultBottomMenu defaultBottomMenu) {
        isShoDelivery = false;
        productAdapter = new ProductAdapter();
        showOrderForRestaurant = null;
    }
}