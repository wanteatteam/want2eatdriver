package jdroidcoder.ua.wanteatdriver.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import jdroidcoder.ua.wanteatdriver.R;
import jdroidcoder.ua.wanteatdriver.adapter.OrdersAdapter;
import jdroidcoder.ua.wanteatdriver.presenter.FreeStreamPresenter;
import jdroidcoder.ua.wanteatdriver.presenter.impl.FreeStreamPresenterImpl;
import jdroidcoder.ua.wanteatdriver.response.OrderResponse;
import jdroidcoder.ua.wanteatdriver.view.FreeStreamView;

/**
 * Created by jdroidcoder on 17.09.17.
 */
public class FreeStreamFragment extends BaseFragment implements FreeStreamView {
    public static FreeStreamFragment newInstance() {
        return new FreeStreamFragment();
    }

    @BindView(R.id.order_list)
    RecyclerView orderList;
    private OrdersAdapter ordersAdapter;
    public static FreeStreamPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recycler_view_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new FreeStreamPresenterImpl(getContext(), this);
        presenter.loadOrders();
        orderList.setLayoutManager(new LinearLayoutManager(getContext()));
        ordersAdapter = new OrdersAdapter(true);
        orderList.setAdapter(ordersAdapter);
    }

    @Override
    public void setItems(List<OrderResponse> orders) {
        ordersAdapter.setItems(orders);
    }
}