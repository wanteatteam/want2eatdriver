package jdroidcoder.ua.wanteatdriver.fragment;

import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import jdroidcoder.ua.wanteatdriver.R;
import jdroidcoder.ua.wanteatdriver.presenter.NewOrderPresenter;
import jdroidcoder.ua.wanteatdriver.presenter.impl.NewOrderPresenterImpl;
import jdroidcoder.ua.wanteatdriver.response.OrderResponse;
import jdroidcoder.ua.wanteatdriver.view.NewOrderView;

/**
 * Created by jdroidcoder on 21.08.17.
 */
public class NewOrderFragment extends BaseFragment implements NewOrderView {
    private final static String ORDER_KEY = "order_key";

    public static NewOrderFragment newInstance(OrderResponse order) {
        NewOrderFragment newOrderFragment = new NewOrderFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ORDER_KEY, order);
        newOrderFragment.setArguments(bundle);
        return newOrderFragment;
    }

    @BindView(R.id.shop_name)
    TextView shopName;
    @BindView(R.id.shop_address)
    TextView shopAddress;
    @BindView(R.id.need_pay)
    TextView needPay;
    @BindView(R.id.time)
    TextView time;
    private OrderResponse order;
    private NewOrderPresenter newOrderPresenter;
    private Ringtone ringtone = null;
    private boolean isReaction = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_new_order, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        order = (OrderResponse) getArguments().getSerializable(ORDER_KEY);
        assert order != null;
        if (newOrderPresenter == null)
            newOrderPresenter = new NewOrderPresenterImpl(getContext(), this);
        initOrder();
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            ringtone = RingtoneManager.getRingtone(getContext(), notification);
            ringtone.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initOrder() {
        shopName.setText(order.getShopName());
        shopAddress.setText(order.getShopAddress());
        needPay.setText(getString(R.string.currency, order.getPayForRestaurant()));
        time.setText(getString(R.string.time, order.getTime()));
    }

    @Override
    public Integer getOrderId() {
        return order.getOrderId();
    }

    @Override
    public OrderResponse getOrder() {
        return order;
    }

    @OnClick(R.id.decline)
    public void declineOrder() {
        if (ringtone != null) {
            ringtone.stop();
        }
        isReaction = true;
        newOrderPresenter.declineOrder();
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    @OnClick(R.id.accept)
    public void acceptOrder() {
        if (ringtone != null) {
            ringtone.stop();
        }
        isReaction = true;
        newOrderPresenter.acceptOrder();
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (!isReaction) {
            newOrderPresenter.declineOrder();
        }
    }

    @OnClick(R.id.back_button)
    public void back() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @OnClick(R.id.container)
    public void containerClick() {
        if (ringtone != null) {
            ringtone.stop();
        }
    }
}
