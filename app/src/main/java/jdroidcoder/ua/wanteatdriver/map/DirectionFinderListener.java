package jdroidcoder.ua.wanteatdriver.map;

import java.util.List;

import jdroidcoder.ua.wanteatdriver.response.RouteResponse;

/**
 * Created by jdroidcoder on 03.09.17.
 */
public interface DirectionFinderListener {
    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<RouteResponse> route);
}
