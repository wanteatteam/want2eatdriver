package jdroidcoder.ua.wanteatdriver.network;

import java.io.IOException;

/**
 * Created by jdroidcoder on 16.05.17.
 */
public class ApiException extends IOException {

    private String status;
    private String message;

    public ApiException(String status, String message){
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}