package jdroidcoder.ua.wanteatdriver.network;

import jdroidcoder.ua.wanteatdriver.response.BaseResponse;
import jdroidcoder.ua.wanteatdriver.response.MainResponse;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by jdroidcoder on 16.05.17.
 */
public interface ApiService {
    @POST("auth/")
    @FormUrlEncoded
    Observable<BaseResponse<MainResponse>> login(@Field("identificate") String login,
                                                 @Field("password") String password,
                                                 @Field("role_id") String roleId);

    @POST("auth/logout/")
    Observable<BaseResponse<MainResponse>> logout();

    @POST("courier/order-status")
    @FormUrlEncoded
    Observable<BaseResponse<MainResponse>> acceptOrder(@Field("shop_order_id") Integer orderId,
                                                       @Field("status_id") String status);

    @POST("courier/order-status")
    @FormUrlEncoded
    Observable<BaseResponse<MainResponse>> declineOrder(@Field("shop_order_id") Integer orderId,
                                                        @Field("status_id") String status);

    @GET("full_order_info")
    Observable<BaseResponse<MainResponse>> fullOrderInfo(@Query("order_id") Integer orderId);

    @POST("courier/check-order/")
    @FormUrlEncoded
    Observable<BaseResponse<MainResponse>> takeFromRestaurant(@Field("shop_order_id") Integer orderId,
                                                              @Field("status_id") String status);

    @GET("on_place")
    Observable<BaseResponse<MainResponse>> onPlace(@Query("order_id") Integer orderId);

    @POST("courier/orders-self")
    Observable<BaseResponse<MainResponse>> getMyOrder();

    @POST("courier/orders-free")
    Observable<BaseResponse<MainResponse>> getFreeStream();

    @POST("order/operator-phone/")
    @FormUrlEncoded
    Observable<BaseResponse<MainResponse>> getOperatorPhone(@Field("shop_order_id") Integer orderId);
}