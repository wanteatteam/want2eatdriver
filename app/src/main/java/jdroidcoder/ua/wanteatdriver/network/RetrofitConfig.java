package jdroidcoder.ua.wanteatdriver.network;

import android.text.TextUtils;

import java.io.IOException;

import jdroidcoder.ua.wanteatdriver.User;
import jdroidcoder.ua.wanteatdriver.WantEatApplication;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jdroidcoder on 16.05.17.
 */
public class RetrofitConfig {
    public static String BASE_URL = "http://want2eat.net/api/";
    public static String IMAGE_URL = "http://want2eat.net/images/%s";

    public static ApiService getAdapter() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(new AuthInterceptor())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        return retrofit.create(ApiService.class);
    }

    private static class AuthInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            String token = WantEatApplication.token;
            System.out.println("token = " + token);
            Request original = chain.request();
            if (!TextUtils.isEmpty(token)) {
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Token", token)
                        .method(original.method(), original.body());

                return chain.proceed(requestBuilder.build());
            } else {
                return chain.proceed(original);
            }
        }
    }
}
