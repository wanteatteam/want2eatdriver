package jdroidcoder.ua.wanteatdriver.network;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import jdroidcoder.ua.wanteatdriver.User;
import jdroidcoder.ua.wanteatdriver.WantEatApplication;
import jdroidcoder.ua.wanteatdriver.response.MainResponse;
import jdroidcoder.ua.wanteatdriver.utils.Util;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.OnErrorFailedException;
import rx.schedulers.Schedulers;

/**
 * Created by jdroidcoder on 16.05.17.
 */
public class RetrofitSubscriber<T> extends Subscriber<T> {
    private Context context = null;

    @Override
    public void onNext(T t) {
    }

    @Override
    public void onError(Throwable e) {
        if (e instanceof OnErrorFailedException) {
            e.printStackTrace();
        } else {
            if (context == null)
                context = WantEatApplication.getApplication();
            String message = e.getMessage();
            try {
                if (message.equals("Access denied")) {
                    WantEatApplication.token = null;
                    User.setToken(null);
                    RetrofitConfig.getAdapter().login(WantEatApplication.login, WantEatApplication.password, "courier")
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .unsubscribeOn(Schedulers.io())
                            .flatMap(new ValidateServerResponseOrThrow<>())
                            .subscribe(new RetrofitSubscriber<MainResponse>() {
                                @Override
                                public void onNext(MainResponse main) {
                                    Util.saveToken(context, main.getUser().getToken());
                                    main.getUser().save();
                                }
                            });
                    return;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            if (e instanceof ApiException && !TextUtils.isEmpty(e.getMessage())) {
                message = e.getMessage().trim();
            }
            if (!(e instanceof NullPointerException)) {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
            onCompleted();
            e.printStackTrace();
        }
    }

    @Override
    public void onCompleted() {

    }
}