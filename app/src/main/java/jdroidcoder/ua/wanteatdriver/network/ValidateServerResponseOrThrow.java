package jdroidcoder.ua.wanteatdriver.network;

import android.text.TextUtils;

import jdroidcoder.ua.wanteatdriver.response.BaseResponse;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by jdroidcoder on 16.05.17.
 */
public class ValidateServerResponseOrThrow<T> implements Func1<BaseResponse<T>, Observable<T>> {

    private static final String SUCCESS_STATUS = "success";

    @Override
    public Observable<T> call(BaseResponse<T> tBaseResponse) {
        if (!TextUtils.isEmpty(tBaseResponse.getStatus()) && !SUCCESS_STATUS.equals(tBaseResponse.getStatus())) {
            return Observable.error(new ApiException(tBaseResponse.getStatus(), tBaseResponse.getMessage()));
        }

        return Observable.just(tBaseResponse.getData());
    }
}

