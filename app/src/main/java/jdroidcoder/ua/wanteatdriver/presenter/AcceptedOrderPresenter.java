package jdroidcoder.ua.wanteatdriver.presenter;

/**
 * Created by jdroidcoder on 21.08.17.
 */
public interface AcceptedOrderPresenter {

    void takeFromRestaurant();

    void onPlace();
}
