package jdroidcoder.ua.wanteatdriver.presenter;

/**
 * Created by jdroidcoder on 07.08.17.
 */
public interface LoginPresenter {
    void login();
}
