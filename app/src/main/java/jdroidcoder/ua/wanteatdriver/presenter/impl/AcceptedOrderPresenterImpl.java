package jdroidcoder.ua.wanteatdriver.presenter.impl;

import android.content.Context;

import org.greenrobot.eventbus.EventBus;

import jdroidcoder.ua.wanteatdriver.activity.MainActivity;
import jdroidcoder.ua.wanteatdriver.event.UpdateTabs;
import jdroidcoder.ua.wanteatdriver.network.RetrofitSubscriber;
import jdroidcoder.ua.wanteatdriver.network.ValidateServerResponseOrThrow;
import jdroidcoder.ua.wanteatdriver.presenter.AcceptedOrderPresenter;
import jdroidcoder.ua.wanteatdriver.response.MainResponse;
import jdroidcoder.ua.wanteatdriver.view.AcceptedOrderView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by jdroidcoder on 21.08.17.
 */
public class AcceptedOrderPresenterImpl extends BasePresenter implements AcceptedOrderPresenter {
    private AcceptedOrderView acceptedOrderView;

    public AcceptedOrderPresenterImpl(Context context, AcceptedOrderView acceptedOrderView) {
        super(context);
        this.acceptedOrderView = acceptedOrderView;
    }

    @Override
    public void takeFromRestaurant() {
        apiService.takeFromRestaurant(acceptedOrderView.getOrder().getOrderId(), "shop_order_get_for_courier")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::startLoading)
                .doOnCompleted(this::stopLoading)
                .unsubscribeOn(Schedulers.io())
                .flatMap(new ValidateServerResponseOrThrow<>())
                .subscribe(new RetrofitSubscriber<MainResponse>() {
                    @Override
                    public void onNext(MainResponse main) {
                        MainActivity.address = acceptedOrderView.getOrder().getClientAddress().getStreet() +
                                ", " + acceptedOrderView.getOrder().getClientAddress().getBuild() +
                                "Рівне, Рівненська область, 33000";
                        acceptedOrderView.hideTakeFromRestaurant();
                        EventBus.getDefault().post(new UpdateTabs());
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        stopLoading();
                    }
                });
    }

    @Override
    public void onPlace() {
        apiService.declineOrder(acceptedOrderView.getOrder().getOrderId(), "shop_order_get_for_client")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::startLoading)
                .doOnCompleted(this::stopLoading)
                .unsubscribeOn(Schedulers.io())
                .flatMap(new ValidateServerResponseOrThrow<>())
                .subscribe(new RetrofitSubscriber<MainResponse>() {
                    @Override
                    public void onNext(MainResponse main) {
                        EventBus.getDefault().post(new UpdateTabs());
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        stopLoading();
                    }
                });
    }
}
