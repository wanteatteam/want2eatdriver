package jdroidcoder.ua.wanteatdriver.presenter.impl;

import android.app.ProgressDialog;
import android.content.Context;

import jdroidcoder.ua.wanteatdriver.R;
import jdroidcoder.ua.wanteatdriver.network.ApiService;
import jdroidcoder.ua.wanteatdriver.network.RetrofitConfig;

/**
 * Created by jdroidcoder on 07.08.17.
 */
class BasePresenter {
    private ProgressDialog progressDialog;
    protected ApiService apiService;
    protected Context context;

    protected BasePresenter(Context context) {
        this.context =context;
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("");
        progressDialog.setMessage("");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        this.apiService = RetrofitConfig.getAdapter();
    }

    protected void startLoading() {
        if (!progressDialog.isShowing()) {
            progressDialog.show();
            progressDialog.setContentView(R.layout.custom_progress_layout);
        }
    }

    protected void stopLoading() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
