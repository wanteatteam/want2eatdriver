package jdroidcoder.ua.wanteatdriver.presenter.impl;

import android.content.Context;

import jdroidcoder.ua.wanteatdriver.network.RetrofitSubscriber;
import jdroidcoder.ua.wanteatdriver.network.ValidateServerResponseOrThrow;
import jdroidcoder.ua.wanteatdriver.presenter.FreeStreamPresenter;
import jdroidcoder.ua.wanteatdriver.response.MainResponse;
import jdroidcoder.ua.wanteatdriver.view.FreeStreamView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by jdroidcoder on 18.09.17.
 */
public class FreeStreamPresenterImpl extends BasePresenter implements FreeStreamPresenter {
    private FreeStreamView freeStreamView;

    public FreeStreamPresenterImpl(Context context, FreeStreamView freeStreamView) {
        super(context);
        this.freeStreamView = freeStreamView;
    }

    @Override
    public void loadOrders() {
        apiService.getFreeStream()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .flatMap(new ValidateServerResponseOrThrow<>())
                .subscribe(new RetrofitSubscriber<MainResponse>() {
                    @Override
                    public void onNext(MainResponse main) {
                        freeStreamView.setItems(main.getOrders());
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                    }
                });
    }
}
