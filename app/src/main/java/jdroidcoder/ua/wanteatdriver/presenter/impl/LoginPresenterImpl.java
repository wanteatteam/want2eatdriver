package jdroidcoder.ua.wanteatdriver.presenter.impl;

import android.content.Context;

import jdroidcoder.ua.wanteatdriver.User;
import jdroidcoder.ua.wanteatdriver.WantEatApplication;
import jdroidcoder.ua.wanteatdriver.network.RetrofitSubscriber;
import jdroidcoder.ua.wanteatdriver.network.ValidateServerResponseOrThrow;
import jdroidcoder.ua.wanteatdriver.presenter.LoginPresenter;
import jdroidcoder.ua.wanteatdriver.response.MainResponse;
import jdroidcoder.ua.wanteatdriver.response.UserResponse;
import jdroidcoder.ua.wanteatdriver.utils.Util;
import jdroidcoder.ua.wanteatdriver.view.LoginView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by jdroidcoder on 07.08.17.
 */
public class LoginPresenterImpl extends BasePresenter implements LoginPresenter {
    private LoginView loginView;

    public LoginPresenterImpl(Context context, LoginView loginView) {
        super(context);
        this.loginView = loginView;
    }

    @Override
    public void login() {
        String login = loginView.getLogin();
        String password = loginView.getPassword();
        if (login.isEmpty()) {
            loginView.invalidLogin();
        } else if (password.isEmpty()) {
            loginView.invalidPassword();
        } else {
            User.setToken(null);
            apiService.login(login, password, "courier")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(this::startLoading)
                    .doOnCompleted(this::stopLoading)
                    .unsubscribeOn(Schedulers.io())
                    .flatMap(new ValidateServerResponseOrThrow<>())
                    .subscribe(new RetrofitSubscriber<MainResponse>() {
                        @Override
                        public void onNext(MainResponse main) {
                            Util.saveToken(context, main.getUser().getToken());
                            WantEatApplication.login = login;
                            WantEatApplication.password = password;
                            main.getUser().save();
                            loginView.logged();
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (e.getMessage().equals("Token already exists")) {
                                logout();
                            } else {
                                super.onError(e);
                                stopLoading();
                            }
                        }
                    });
        }
    }

    public void logout() {
        apiService.logout()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::startLoading)
                .doOnCompleted(this::stopLoading)
                .unsubscribeOn(Schedulers.io())
                .flatMap(new ValidateServerResponseOrThrow<>())
                .subscribe(new RetrofitSubscriber<MainResponse>() {
                    @Override
                    public void onNext(MainResponse main) {
                        Util.saveToken(context, null);
                        WantEatApplication.token = null;
                        WantEatApplication.login = null;
                        WantEatApplication.password = null;
                        new UserResponse().clear();
                        login();
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        Util.saveToken(context, null);
                        new UserResponse().clear();
                        stopLoading();
                    }
                });
    }
}
