package jdroidcoder.ua.wanteatdriver.presenter.impl;

import android.content.Context;

import jdroidcoder.ua.wanteatdriver.network.RetrofitSubscriber;
import jdroidcoder.ua.wanteatdriver.network.ValidateServerResponseOrThrow;
import jdroidcoder.ua.wanteatdriver.presenter.MainPresenter;
import jdroidcoder.ua.wanteatdriver.response.MainResponse;
import jdroidcoder.ua.wanteatdriver.response.UserResponse;
import jdroidcoder.ua.wanteatdriver.view.MainView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by jdroidcoder on 08.08.17.
 */
public class MainPresenterImpl extends BasePresenter implements MainPresenter {
    private MainView mainView;

    public MainPresenterImpl(Context context, MainView mainView) {
        super(context);
        this.mainView = mainView;
    }

    @Override
    public void logout() {
        apiService.logout()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::startLoading)
                .doOnCompleted(this::stopLoading)
                .unsubscribeOn(Schedulers.io())
                .flatMap(new ValidateServerResponseOrThrow<>())
                .subscribe(new RetrofitSubscriber<MainResponse>() {
                    @Override
                    public void onNext(MainResponse main) {
                        new UserResponse().clear();
                        mainView.logout();
                    }

                    @Override
                    public void onError(Throwable e) {
                        new UserResponse().clear();
                        mainView.logout();
                        stopLoading();
                    }
                });
    }
}
