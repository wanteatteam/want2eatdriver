package jdroidcoder.ua.wanteatdriver.presenter.impl;

import android.content.Context;

import jdroidcoder.ua.wanteatdriver.network.RetrofitSubscriber;
import jdroidcoder.ua.wanteatdriver.network.ValidateServerResponseOrThrow;
import jdroidcoder.ua.wanteatdriver.presenter.MyOrderPresenter;
import jdroidcoder.ua.wanteatdriver.response.MainResponse;
import jdroidcoder.ua.wanteatdriver.view.MyOrderView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by jdroidcoder on 18.09.17.
 */
public class MyOrderPresenterImpl extends BasePresenter implements MyOrderPresenter {
    private MyOrderView myOrderView;

    public MyOrderPresenterImpl(Context context, MyOrderView myOrderView) {
        super(context);
        this.myOrderView = myOrderView;
    }

    @Override
    public void loadOrders() {
        apiService.getMyOrder()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .flatMap(new ValidateServerResponseOrThrow<>())
                .subscribe(new RetrofitSubscriber<MainResponse>() {
                    @Override
                    public void onNext(MainResponse main) {
                        myOrderView.setItems(main.getOrders());
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                    }
                });
    }
}
