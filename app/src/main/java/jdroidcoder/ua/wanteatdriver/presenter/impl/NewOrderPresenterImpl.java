package jdroidcoder.ua.wanteatdriver.presenter.impl;

import android.content.Context;

import org.greenrobot.eventbus.EventBus;

import jdroidcoder.ua.wanteatdriver.activity.MainActivity;
import jdroidcoder.ua.wanteatdriver.event.ShowCallToClientButton;
import jdroidcoder.ua.wanteatdriver.event.ShowMapButton;
import jdroidcoder.ua.wanteatdriver.event.ShowOrderButton;
import jdroidcoder.ua.wanteatdriver.event.ShowOrderForRestaurant;
import jdroidcoder.ua.wanteatdriver.event.UpdateTabs;
import jdroidcoder.ua.wanteatdriver.fragment.AcceptedOrderFragment;
import jdroidcoder.ua.wanteatdriver.network.RetrofitSubscriber;
import jdroidcoder.ua.wanteatdriver.network.ValidateServerResponseOrThrow;
import jdroidcoder.ua.wanteatdriver.presenter.NewOrderPresenter;
import jdroidcoder.ua.wanteatdriver.response.MainResponse;
import jdroidcoder.ua.wanteatdriver.view.NewOrderView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by jdroidcoder on 21.08.17.
 */
public class NewOrderPresenterImpl extends BasePresenter implements NewOrderPresenter {
    private NewOrderView newOrderView;

    public NewOrderPresenterImpl(Context context, NewOrderView newOrderView) {
        super(context);
        this.newOrderView = newOrderView;
    }

    @Override
    public void acceptOrder() {
        apiService.acceptOrder(newOrderView.getOrderId(), "accept")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::startLoading)
                .doOnCompleted(this::stopLoading)
                .unsubscribeOn(Schedulers.io())
                .flatMap(new ValidateServerResponseOrThrow<>())
                .subscribe(new RetrofitSubscriber<MainResponse>() {
                    @Override
                    public void onNext(MainResponse main) {
                        MainActivity.address = main.getShopOrder().getShop().getStreet() + ", "
                                + main.getShopOrder().getShop().getBuild() + "Рівне, Рівненська область, 33000";
                        newOrderView.getOrder().setClientAddress(main.getShopOrder().getClientAddress());
                        newOrderView.getOrder().setDeliveryPrice(main.getShopOrder().getDeliveryPrice());
                        newOrderView.getOrder().setProducts(main.getShopOrder().getProducts());
                        newOrderView.getOrder().setDeliveryPrice(main.getShopOrder().getDeliveryPrice());
                        newOrderView.getOrder().setClientPhone(main.getShopOrder().getClientPhone());
                        ((MainActivity) context).showNewOrder(AcceptedOrderFragment.newInstance(main.getShopOrder()));
                        EventBus.getDefault().post(new ShowMapButton());
                        EventBus.getDefault().post(new ShowCallToClientButton(main.getShopOrder().getClientPhone()));
                        EventBus.getDefault().post(new ShowOrderForRestaurant(main.getShopOrder().getDeliveryPrice(), main.getShopOrder().getProducts()));
                        EventBus.getDefault().post(new ShowOrderButton());
                        EventBus.getDefault().post(new UpdateTabs());
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        stopLoading();
                    }
                });
    }

    @Override
    public void declineOrder() {
        apiService.declineOrder(newOrderView.getOrderId(), "not_accept")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::startLoading)
                .doOnCompleted(this::stopLoading)
                .unsubscribeOn(Schedulers.io())
                .flatMap(new ValidateServerResponseOrThrow<>())
                .subscribe(new RetrofitSubscriber<MainResponse>() {
                    @Override
                    public void onNext(MainResponse main) {
                        EventBus.getDefault().post(new UpdateTabs());
                        ((MainActivity) context).removeFragment();
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        stopLoading();
                    }
                });
    }
}
