package jdroidcoder.ua.wanteatdriver.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jdroidcoder on 28.08.17.
 */
public class AddressResponse implements Serializable {
    private static final Long serialVersionUID = 2L;
    @SerializedName("name")
    private String name;
    @SerializedName("city")
    private String city;
    @SerializedName("street")
    private String street;
    @SerializedName("build")
    private String build;
    @SerializedName("entrance")
    private String entrance;
    @SerializedName("domophone_code")
    private String domophoneCode;
    @SerializedName("level")
    private String level;
    @SerializedName("room")
    private String room;
    @SerializedName("client")
    ClientResponse client;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuild() {
        return build;
    }

    public void setBuild(String build) {
        this.build = build;
    }

    public String getEntrance() {
        return entrance;
    }

    public void setEntrance(String entrance) {
        this.entrance = entrance;
    }

    public String getDomophoneCode() {
        return domophoneCode;
    }

    public void setDomophoneCode(String domophoneCode) {
        this.domophoneCode = domophoneCode;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public ClientResponse getClient() {
        return client;
    }

    public void setClientResponse(ClientResponse client) {
        this.client = client;
    }
}
