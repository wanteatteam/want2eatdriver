package jdroidcoder.ua.wanteatdriver.response;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

/**
 * Created by jdroidcoder on 18.07.17.
 */
public class BaseResponse<T> {
    @SerializedName("method_name")
    private String methodName;
    @Getter
    private T data;
    @Getter
    private String status;
    @Getter
    private Integer code;
    @Getter
    private String message;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}

