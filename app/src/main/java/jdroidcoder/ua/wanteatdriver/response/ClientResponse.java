package jdroidcoder.ua.wanteatdriver.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jdroidcoder on 20.09.17.
 */
public class ClientResponse implements Serializable {
    private static final Long serialVersionUID = 3L;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("phone")
    private String phone;
    @SerializedName("last_name")
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
