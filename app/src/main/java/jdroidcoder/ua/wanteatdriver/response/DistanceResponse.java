package jdroidcoder.ua.wanteatdriver.response;

/**
 * Created by jdroidcoder on 03.09.17.
 */
public class DistanceResponse {
    private String text;
    private int value;

    public DistanceResponse(String text, int value) {
        this.text = text;
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
