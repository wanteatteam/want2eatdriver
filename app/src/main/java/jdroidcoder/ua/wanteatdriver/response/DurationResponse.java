package jdroidcoder.ua.wanteatdriver.response;

/**
 * Created by jdroidcoder on 03.09.17.
 */
public class DurationResponse {
    private String text;
    private int value;

    public DurationResponse(String text, int value) {
        this.text = text;
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
