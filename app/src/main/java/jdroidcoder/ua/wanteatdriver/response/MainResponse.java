package jdroidcoder.ua.wanteatdriver.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jdroidcoder on 07.08.17.
 */
public class MainResponse {
    @SerializedName("user")
    private UserResponse user;
    @SerializedName("shopOrder")
    private OrderResponse shopOrder;
    @SerializedName("orders")
    private List<OrderResponse> orders;
    @SerializedName("operator_phone")
    private String operatorPhone;

    public UserResponse getUser() {
        return user;
    }

    public void setUser(UserResponse user) {
        this.user = user;
    }

    public OrderResponse getShopOrder() {
        return shopOrder;
    }

    public void setShopOrder(OrderResponse shopOrder) {
        this.shopOrder = shopOrder;
    }

    public List<OrderResponse> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderResponse> orders) {
        this.orders = orders;
    }

    public String getOperatorPhone() {
        return operatorPhone;
    }

    public void setOperatorPhone(String operatorPhone) {
        this.operatorPhone = operatorPhone;
    }
}
