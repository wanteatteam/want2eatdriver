package jdroidcoder.ua.wanteatdriver.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jdroidcoder on 21.08.17.
 */
public class OrderResponse implements Serializable {
    private static final Long serialVersionUID = 1L;

    @SerializedName("id")
    private Integer orderId;
    @SerializedName("shop_name")
    private String shopName;
    @SerializedName("shop_address")
    private String shopAddress;
    @SerializedName("pay_for_restaurant")
    private Double payForRestaurant;
    @SerializedName("time")
    private Integer time;
    @SerializedName("clientOrder")
    private AddressResponse clientAddress;
    @SerializedName("shop")
    private AddressResponse shop;
    @SerializedName("client_phone")
    private String clientPhone;
    @SerializedName("delivery_price")
    private Double deliveryPrice;
    @SerializedName("productOrders")
    private List<ProductResponse> products;
    @SerializedName("total_price_for_shop")
    private Double totalPriceForShop;
    @SerializedName("total_price_for_client")
    private Double totalPriceForClient;
    @SerializedName("finish_delivery_at")
    private String finishDeliveryAt;
    @SerializedName("finish_cook_at")
    private String finishCookAt;
    @SerializedName("status_id")
    private String statusId;

    public String getFinishCookAt() {
        return finishCookAt;
    }

    public void setFinishCookAt(String finishCookAt) {
        this.finishCookAt = finishCookAt;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public Double getPayForRestaurant() {
        return payForRestaurant;
    }

    public void setPayForRestaurant(Double payForRestaurant) {
        this.payForRestaurant = payForRestaurant;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public AddressResponse getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(AddressResponse clientAddress) {
        this.clientAddress = clientAddress;
    }

    public AddressResponse getShop() {
        return shop;
    }

    public void setShop(AddressResponse shop) {
        this.shop = shop;
    }

    public Double getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(Double deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    public List<ProductResponse> getProducts() {
        return products;
    }

    public void setProducts(List<ProductResponse> products) {
        this.products = products;
    }

    public String getClientPhone() {
        return clientPhone;
    }

    public void setClientPhone(String clientPhone) {
        this.clientPhone = clientPhone;
    }

    public Double getTotalPriceForShop() {
        return totalPriceForShop;
    }

    public void setTotalPriceForShop(Double totalPriceForShop) {
        this.totalPriceForShop = totalPriceForShop;
    }

    public Double getTotalPriceForClient() {
        return totalPriceForClient;
    }

    public void setTotalPriceForClient(Double totalPriceForClient) {
        this.totalPriceForClient = totalPriceForClient;
    }

    public String getFinishDeliveryAt() {
        return finishDeliveryAt;
    }

    public void setFinishDeliveryAt(String finishDeliveryAt) {
        this.finishDeliveryAt = finishDeliveryAt;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }
}
