package jdroidcoder.ua.wanteatdriver.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jdroidcoder on 29.08.17.
 */
public class ProductResponse implements Serializable {
    private static final Long serialVersionUID = 6L;

    @SerializedName("name")
    private String productName;
    @SerializedName("size")
    private String size;
    @SerializedName("count")
    private Integer count;
    @SerializedName("price_pack")
    private Double packPrice;
    @SerializedName("price")
    private Double price;
    @SerializedName("price_base")
    private Double priceBase;
    @SerializedName("product_unit_name")
    private String productUnitName;

    public Double getPriceBase() {
        return priceBase;
    }

    public void setPriceBase(Double priceBase) {
        this.priceBase = priceBase;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Double getPackPrice() {
        return packPrice;
    }

    public void setPackPrice(Double packPrice) {
        this.packPrice = packPrice;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getProductUnitName() {
        return productUnitName;
    }

    public void setProductUnitName(String productUnitName) {
        this.productUnitName = productUnitName;
    }
}
