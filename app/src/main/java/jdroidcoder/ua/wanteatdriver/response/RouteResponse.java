package jdroidcoder.ua.wanteatdriver.response;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by jdroidcoder on 03.09.17.
 */
public class RouteResponse {
    private DistanceResponse distance;
    private DurationResponse duration;
    private String endAddress;
    private LatLng endLocation;
    private String startAddress;
    private LatLng startLocation;
    private List<LatLng> points;

    public DistanceResponse getDistance() {
        return distance;
    }

    public void setDistance(DistanceResponse distance) {
        this.distance = distance;
    }

    public DurationResponse getDuration() {
        return duration;
    }

    public void setDuration(DurationResponse duration) {
        this.duration = duration;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    public LatLng getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(LatLng endLocation) {
        this.endLocation = endLocation;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public LatLng getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(LatLng startLocation) {
        this.startLocation = startLocation;
    }

    public List<LatLng> getPoints() {
        return points;
    }

    public void setPoints(List<LatLng> points) {
        this.points = points;
    }
}
