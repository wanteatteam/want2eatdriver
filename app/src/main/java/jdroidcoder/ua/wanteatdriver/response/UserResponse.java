package jdroidcoder.ua.wanteatdriver.response;

import com.google.gson.annotations.SerializedName;

import jdroidcoder.ua.wanteatdriver.User;

/**
 * Created by jdroidcoder on 21.07.17.
 */
public class UserResponse {
    @SerializedName("id")
    private Integer id;
    @SerializedName("role_id")
    private String roleId;
    @SerializedName("shop_id")
    private Integer shopId;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("phone")
    private String phone;
    @SerializedName("email")
    private String email;
    @SerializedName("token")
    private String token;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void save() {
        User.setToken(getToken());
        User.setEmail(getEmail());
        User.setFirstName(getFirstName());
        User.setLastName(getLastName());
        User.setId(getId());
        User.setShopId(getShopId());
        User.setRoleId(getRoleId());
        User.setPhone(getPhone());
    }

    public void clear() {
        User.setToken(null);
        User.setEmail(null);
        User.setFirstName(null);
        User.setLastName(null);
        User.setId(null);
        User.setShopId(null);
        User.setRoleId(null);
        User.setPhone(null);
    }
}
