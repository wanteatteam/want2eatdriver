package jdroidcoder.ua.wanteatdriver.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.concurrent.TimeUnit;

import jdroidcoder.ua.wanteatdriver.fragment.FreeStreamFragment;
import jdroidcoder.ua.wanteatdriver.fragment.MyOrderFragment;

/**
 * Created by jdroidcoder on 29.07.17.
 */
public class TrackingOrderService extends IntentService {

    public TrackingOrderService() {
        super("TrackingOrderService");
    }

    private boolean isRun = true;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            FreeStreamFragment.presenter.loadOrders();
            MyOrderFragment.presenter.loadOrders();
        } catch (Exception e) {
        }
        try {
            TimeUnit.MINUTES.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (isRun)
            onHandleIntent(intent);
    }

    @Override
    public void onDestroy() {
        isRun = false;
        super.onDestroy();
    }
}
