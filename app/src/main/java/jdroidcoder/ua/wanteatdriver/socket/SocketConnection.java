package jdroidcoder.ua.wanteatdriver.socket;

import android.os.Handler;
import android.os.Looper;

import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketHandler;

/**
 * Created by jdroidcoder on 17.05.17.
 */
public class SocketConnection {

    private static final int RECONNECT_DELAY = 1000;

    private WebSocketConnection socketConnection;
    private Handler handler = new Handler(Looper.getMainLooper());

    private ConnectionListener connectionListener;

    private String url;

    private boolean isClosed = false;

    public SocketConnection(String url) {
        this.url = url;
    }

    public boolean openConnection() {

        if (socketConnection != null) {
            closeConnection();
        }

        isClosed = false;

        socketConnection = new WebSocketConnection();

        try {
            socketConnection.connect(url, new SocketHandler());

        } catch (WebSocketException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public void closeConnection() {
        isClosed = true;

        handler.removeCallbacks(reconnectTask);

        if (socketConnection != null && socketConnection.isConnected()) {
            socketConnection.disconnect();
        }
    }

    public WebSocketConnection getSocketConnection() {
        return socketConnection;
    }

    private class SocketHandler extends WebSocketHandler {

        @Override
        public void onOpen() {
            super.onOpen();

            if (connectionListener != null) {
                connectionListener.onSocketConnected();
            }
        }

        @Override
        public void onClose(int code, String reason) {
            super.onClose(code, reason);

            if (!isClosed) {

                if (connectionListener != null) {
                    connectionListener.onSocketDisconnected();
                }

                handler.postDelayed(reconnectTask, RECONNECT_DELAY);
            }
        }

        @Override
        public void onTextMessage(String payload) {
            super.onTextMessage(payload);
            if (connectionListener != null) {
                connectionListener.onServerDataReceived(payload);
            }
        }
    }

    private Runnable reconnectTask = () -> openConnection();

    public boolean isClosed() {
        return isClosed;
    }

    public void setConnectionListener(ConnectionListener connectionListener) {
        this.connectionListener = connectionListener;
    }

    public interface ConnectionListener {

        void onServerDataReceived(String payload);

        void onSocketDisconnected();

        void onSocketConnected();
    }
}
