package jdroidcoder.ua.wanteatdriver.socket;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.eventbus.EventBus;

import de.tavendo.autobahn.WebSocketConnection;
import jdroidcoder.ua.wanteatdriver.User;
import jdroidcoder.ua.wanteatdriver.activity.MainActivity;
import jdroidcoder.ua.wanteatdriver.event.HideNotification;
import jdroidcoder.ua.wanteatdriver.event.ShowCallToClientButton;
import jdroidcoder.ua.wanteatdriver.event.ShowMapButton;
import jdroidcoder.ua.wanteatdriver.event.ShowOrderButton;
import jdroidcoder.ua.wanteatdriver.event.ShowOrderForRestaurant;
import jdroidcoder.ua.wanteatdriver.fragment.AcceptedOrderFragment;
import jdroidcoder.ua.wanteatdriver.fragment.FreeStreamFragment;
import jdroidcoder.ua.wanteatdriver.fragment.MyOrderFragment;
import jdroidcoder.ua.wanteatdriver.fragment.NewOrderFragment;
import jdroidcoder.ua.wanteatdriver.response.OrderResponse;


/**
 * Created by jdroidcoder on 17.05.17.
 */
public class SocketManager implements SocketConnection.ConnectionListener {

    private enum SocketStatuses {
        TIMEOUT("shop_order_timeout"),
        NEW("shop_order_new");

        private final String text;

        SocketStatuses(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    private static final String URL_FORMAT = "%s:%s?token=%s";
    private static final String FIELD_METHOD_NAME = "action";
    private static final String METHOD_NEW_ORDER = "new_order";
    private static final String METHOD_AUTO_DECLINE_ORDER = "auto_decline";
    private static final String METHOD_ACCEPTED_ORDER = "accepted_order";

    private SocketConnection socketConnection;
    private Context context;
    private Gson gson;

    public SocketManager(Context context) {
        this.gson = new GsonBuilder().create();
        this.context = context;
        socketConnection = new SocketConnection(String.format(URL_FORMAT, "ws://want2eat.net", "8080", User.getToken()));
        socketConnection.setConnectionListener(this);
    }

    public void openConnection() {
        socketConnection.openConnection();
    }

    public void closeConnection() {
        socketConnection.closeConnection();
    }

    public void exit() {
        socketConnection.closeConnection();
    }

    @Override
    public void onServerDataReceived(String data) {
        System.out.println("Socket receive = " + data);
        if (TextUtils.isEmpty(data) || !data.startsWith("{")) {
            return;
        }

        SocketModel socketModel = gson.fromJson(data, SocketModel.class);

        if (TextUtils.isEmpty(socketModel.getAction())) {
            return;
        }
        switch (socketModel.getAction()) {
            case METHOD_NEW_ORDER:
                ((MainActivity) context).showNewOrder(NewOrderFragment.newInstance(socketModel.getOrder()));
                break;
            case METHOD_ACCEPTED_ORDER:
//                ((MainActivity) context).showNewOrder(AcceptedOrderFragment.newInstance(socketModel.getOrder()));
//                EventBus.getDefault().post(new ShowMapButton());
//                EventBus.getDefault().post(new ShowCallToClientButton(socketModel.getOrder().getClientPhone()));
//                EventBus.getDefault().post(new ShowOrderForRestaurant(socketModel.getOrder().getDeliveryPrice(), socketModel.getOrder().getProducts()));
//                EventBus.getDefault().post(new ShowOrderButton());
                break;
            case METHOD_AUTO_DECLINE_ORDER:
                try {
                    MyOrderFragment.presenter.loadOrders();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                try {
                    FreeStreamFragment.presenter.loadOrders();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                EventBus.getDefault().post(new HideNotification(socketModel.getOrder().getOrderId()));
                break;
        }
    }

    @Override
    public void onSocketDisconnected() {
        if (!socketConnection.isClosed()) {
        }
    }

    public WebSocketConnection getSocketConnection() {
        return socketConnection.getSocketConnection();
    }

    @Override
    public void onSocketConnected() {
        System.out.println("onSocketConnected");
    }

    private class SocketModel {
        @SerializedName("action")
        private String action;
        @SerializedName("order_model")
        private OrderResponse order;

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public OrderResponse getOrder() {
            return order;
        }

        public void setOrder(OrderResponse order) {
            this.order = order;
        }
    }
}
