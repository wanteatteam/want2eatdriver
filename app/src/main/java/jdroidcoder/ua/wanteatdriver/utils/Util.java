package jdroidcoder.ua.wanteatdriver.utils;

import android.content.Context;

/**
 * Created by jdroidcoder on 29.08.17.
 */
public class Util {
    private static final String TOKEN_FILE_KEY = "token_file_key";
    private static final String TOKEN_KEY = "token_key";

    public static void saveToken(Context context, String token) {
        context.getSharedPreferences(TOKEN_FILE_KEY, Context.MODE_PRIVATE).edit().putString(TOKEN_KEY, token).apply();
    }

    public static String loadToken(Context context) {
        return context.getSharedPreferences(TOKEN_FILE_KEY, Context.MODE_PRIVATE).getString(TOKEN_KEY, "");
    }
}
