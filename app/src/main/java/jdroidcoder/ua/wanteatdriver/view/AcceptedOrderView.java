package jdroidcoder.ua.wanteatdriver.view;

import jdroidcoder.ua.wanteatdriver.response.OrderResponse;

/**
 * Created by jdroidcoder on 21.08.17.
 */
public interface AcceptedOrderView {
    void init(OrderResponse order);

    OrderResponse getOrder();

    void hideTakeFromRestaurant();
}
