package jdroidcoder.ua.wanteatdriver.view;

import java.util.List;

import jdroidcoder.ua.wanteatdriver.response.OrderResponse;

/**
 * Created by jdroidcoder on 18.09.17.
 */
public interface FreeStreamView {
    void setItems(List<OrderResponse> orders);
}
