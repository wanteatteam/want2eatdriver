package jdroidcoder.ua.wanteatdriver.view;

/**
 * Created by jdroidcoder on 07.08.17.
 */
public interface LoginView {
    String getLogin();

    String getPassword();

    void logged();

    void invalidLogin();

    void invalidPassword();
}
