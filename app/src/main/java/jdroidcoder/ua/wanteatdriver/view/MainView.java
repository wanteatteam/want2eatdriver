package jdroidcoder.ua.wanteatdriver.view;

import android.support.v4.app.Fragment;

/**
 * Created by jdroidcoder on 08.08.17.
 */
public interface MainView {
    void logout();

    void showNewOrder(Fragment fragment);

    void removeFragment();
}
