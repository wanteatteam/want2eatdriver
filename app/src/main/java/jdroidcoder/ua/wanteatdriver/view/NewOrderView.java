package jdroidcoder.ua.wanteatdriver.view;

import jdroidcoder.ua.wanteatdriver.response.OrderResponse;

/**
 * Created by jdroidcoder on 21.08.17.
 */
public interface NewOrderView {
    Integer getOrderId();

    OrderResponse getOrder();
}
